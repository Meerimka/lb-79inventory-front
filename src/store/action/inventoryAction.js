import axios from '../../axios-api';

export const FETCH_INVENTORIES_SUCCESS ='FETCH_INVENTORIES_SUCCESS';
export const FETCH_CATEGORIES_SUCCESS ='FETCH_CATEGORIES_SUCCESS';
export const FETCH_PLACES_SUCCESS ='FETCH_PLACES_SUCCESS';
export const CREATE_INVENTORY_SUCCESS ='CREATE_INVENTORY_SUCCESS';
export const FETCH_FAIL ='FETCH_FAIL';

export const fetchInventoriessSuccess = inventories =>({type: FETCH_INVENTORIES_SUCCESS, inventories});
export const fetchCategoriesSuccess = categories =>({type: FETCH_CATEGORIES_SUCCESS, categories});
export const fetchPlacesSuccess = places =>({type: FETCH_PLACES_SUCCESS, places});
export const createInventorySuccess = () =>({type: CREATE_INVENTORY_SUCCESS});

export const fetchFail = error =>({type: FETCH_FAIL});


export const fetchInventories =()=>{
    return dispatch =>{
        return axios.get('/items').then(
            response=> {
                console.log(response.data);
                return dispatch(fetchInventoriessSuccess(response.data))}
        );
    };
};

export const fetchCategories =()=>{
    return dispatch =>{
        return axios.get('/categories').then(
            response=> {
                console.log(response.data);
                return dispatch(fetchCategoriesSuccess(response.data))}
        );
    };
};

export const fetchPlaces =()=>{
    return dispatch =>{
        return axios.get('/places').then(
            response=> {
                console.log(response.data);
                return dispatch(fetchPlacesSuccess(response.data))}
        );
    };
};

export const createInventory = itemData => {
    return dispatch => {
        return axios.post('/items', itemData).then(
            () => dispatch(createInventorySuccess())
        )
    }
};

export const deleteItem = (itemId) => {
    return dispatch => {
        return axios.delete('/items/' + itemId).then(
            response => {
                dispatch(fetchInventories());
            },
            error => dispatch(fetchFail(error))
        );
    }
};


