import {FETCH_CATEGORIES_SUCCESS, FETCH_INVENTORIES_SUCCESS, FETCH_PLACES_SUCCESS} from "../action/inventoryAction";


const initialState ={
    inventories: [],
    categories:[],
    places: []
};

const inventoryReducer = (state=initialState, action)=>{
    switch (action.type) {
        case FETCH_INVENTORIES_SUCCESS:
            return{...state, inventories: action.inventories};
        case FETCH_CATEGORIES_SUCCESS:
            return{...state, categories: action.categories};
        case FETCH_PLACES_SUCCESS:
            return{...state, places: action.places};
        default:
            return state;

    }
};

export default inventoryReducer;