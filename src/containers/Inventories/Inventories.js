import React, {Component, Fragment} from 'react';
import {deleteItem, fetchInventories} from "../../store/action/inventoryAction";
import {Button, Card, CardBody, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {Link} from "react-router-dom";
import {connect} from "react-redux";

class Inventories extends Component {
    state={
        selectedItem:null
    };

    showModal = inventory =>{
            this.setState({selectedItem: inventory})
    };

    hideModal = () =>{
        this.setState({selectedItem: null})
    };


    componentDidMount(){
        this.props.fetchInventories();
    }


    render() {
        return (
            <Fragment>
                <h2>Inventories
                    <Link to="/items/new">
                        <Button
                            color="primary"
                            className="float-right">
                            Add inventory
                        </Button>
                    </Link>
                </h2>

                {this.props.inventories.map(inventory =>(
                    <Card key = {inventory.id} onClick = {()=>this.showModal(inventory)} style={{cursor: "pointer"}}>
                        <CardBody>
                            <Link to={'/items/' + inventory.id}>
                                {inventory.title}
                            </Link>
                        </CardBody>
                    </Card>
                ))}

                <Modal isOpen={!!this.state.selectedItem} toggle={this.hideModal}>
                    {this.state.selectedItem && (
                        <Fragment>
                        <ModalHeader toggle={this.hideModal}>{this.state.selectedItem.title}</ModalHeader>
                        <ModalBody>

                            <img className="img-thumbnail" src={this.state.selectedItem.image} style={{MaxHeight: "80px",
                                display: "block",maxWidth: "80px", marginRight: "20px"}}/>
                            <p>
                                Category:&nbsp;{this.state.selectedItem.category_id}<br/>
                                Place: &nbsp;{this.state.selectedItem.place_id}<br/>
                                Description:&nbsp; {this.state.selectedItem.description}
                            </p>

                        </ModalBody>
                        <ModalFooter>
                        <Button color="primary" >Edit</Button>&nbsp;
                        <Button color="danger" onClick={()=>this.props.delete(this.state.selectedItem.id)} >Delete</Button>&nbsp;
                        <Button color="secondary" onClick={this.hideModal}>Cancel</Button>
                        </ModalFooter>
                        </Fragment>
                    )}

                </Modal>

            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    inventories: state.inventories.inventories
});

const mapDispatchToProps= dispatch =>({
    fetchInventories: () =>dispatch(fetchInventories()),
    delete: (itemId) =>dispatch(deleteItem(itemId))
});

export default connect(mapStateToProps,mapDispatchToProps)(Inventories);