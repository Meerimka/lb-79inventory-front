import React, {Component, Fragment} from 'react';
import {createInventory, fetchCategories, fetchPlaces} from "../../store/action/inventoryAction";
import {connect} from "react-redux";
import InventoryForm from "../../components/InventoryForm/inventoryForm";

class NewItem extends Component {

    componentDidMount(){
        this.props.fetchCategories();
        this.props.fetchPlaces();
    }

    createItem =itemData =>{
        this.props.itemCreated(itemData).then(() =>{
            this.props.history.push('/')
        })
    };

    render() {
        return (
            <Fragment>
                <h2>New Inventory</h2>
                {(this.props.categories.length > 0 && this.props.places.length > 0) ?
                    <InventoryForm onSubmit ={this.createItem} categories={this.props.categories} places={this.props.places}/>
                    :
                    <div>Loading</div>
                }
            </Fragment>
        );
    }
}


const mapStateToProps = state => ({
    categories: state.inventories.categories,
    places:state.inventories.places
});

const mapDispatchToProps = dispatch =>({
    fetchCategories: () => dispatch(fetchCategories()),
    fetchPlaces: () => dispatch(fetchPlaces()),
    itemCreated: itemData => dispatch(createInventory(itemData))
});

export default connect(mapStateToProps,mapDispatchToProps)(NewItem);