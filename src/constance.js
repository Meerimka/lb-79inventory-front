export const CATEGORIES = {
    'fn': 'Furniture',
    'ce': 'Computer equipment',
    'ap': 'Appliances',
};

export const PLACE = {
    'df':'Directors office',
    'Of1':'Office #1',
    'Of2':'Office #2',
    'Of3':'Office #3',

};

export const apiURL = 'http://localhost:8001';