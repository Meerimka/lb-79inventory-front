import React, {Component, Fragment} from 'react';

import './App.css';
import Toolbar from "./components/UI/Toolbar/Toolbar";
import {Container} from "reactstrap";
import {Route, Switch} from "react-router-dom";
import Inventories from "./containers/Inventories/Inventories";
import NewItem from "./containers/NewItem/NewItem";

class App extends Component {
  render() {
    return (
        <Fragment>
            <header>
                <Toolbar/>
            </header>
            <Container style={{marginTop: "20px"}}>
                <Switch>
                    <Route path="/" exact component={Inventories}/>
                    <Route path="/items/new" exact component={NewItem}/>
                </Switch>
            </Container>
        </Fragment>
    );
  }
}

export default App;
