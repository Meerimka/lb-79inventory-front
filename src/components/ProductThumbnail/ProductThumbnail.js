import React from 'react';
import {apiURL} from "../../constance";



const styles = {
    width: '100px',
    height: '100px',
    marginRight: '10px'
};


const ProductThumbnail = (props) => {
    let image = null;

    if(props.image){
        image = apiURL + '/uploads/' + props.image;
    }
    return <img src={image} style={styles} className="img-thumbnail" alt="Product image"/>
};


export default ProductThumbnail;