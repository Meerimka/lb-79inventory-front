import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";


class InventoryForm extends Component {
    constructor(props){
        super(props);
        if(props.inventory){
            this.state = {...props.inventory}
        }else{
            this.state= {
                category_id: '',
                place_id: '',
                title: '',
                description: '',
                image: null
            }
        }
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key =>{
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event =>{
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };



    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <FormGroup row>
                    <Label for="category" sm={2}>Category</Label>
                    <Col sm={10}>
                        <Input type="select" name="category" id="category"
                               value={this.state.category_id} onChange={this.inputChangeHandler}>
                            {this.props.categories.map(categoryId =>(
                                <option key={categoryId} value={categoryId}>{this.props.categories[categoryId]}</option>
                            ))}
                        </Input>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="category" sm={2}>Place</Label>
                    <Col sm={10}>
                        <Input type="select" name="place" id="place"
                               value={this.state.place_id} onChange={this.inputChangeHandler}>
                            {this.props.places.map(placeId =>(
                                <option key={placeId} value={placeId}>{this.props.places[placeId]}</option>
                            ))}
                        </Input>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2} for="title">Title</Label>
                    <Col sm={10}>
                        <Input
                            type="text" required
                            name="title" id="title"
                            placeholder="Enter inventory title"
                            value={this.state.title}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2} for="description">Description</Label>
                    <Col sm={10}>
                        <Input
                            type="textarea" required
                            name="description" id="description"
                            placeholder="Enter description"
                            value={this.state.description}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2} for="image">Image</Label>
                    <Col sm={10}>
                        <Input
                            type="file"
                            name="image" id="image"
                            onChange={this.fileChangeHandler}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col sm={{offset:2, size: 10}}>
                        <Button type="submit" color="primary">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default InventoryForm;